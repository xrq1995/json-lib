package com.example.demo;

import com.example.demo.bean.Person;
import net.sf.json.JSON;
import net.sf.json.JSONObject;
import net.sf.json.xml.XMLSerializer;
import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

  @Test
  public void contextLoads() {
  }

  /**
   * json字符串转java代码
   */
  @Test
  public void jsonToJAVA() throws JSONException {
    System.out.println("json字符串转java代码");
    String jsonStr = "{\"password\":\"\",\"username\":\"张三\"}";
    JSONObject jsonObject = JSONObject.fromObject(jsonStr);
    String username = jsonObject.getString("username");
    String password = jsonObject.optString("password");
    System.out.println("json--->java\n username=" + username
        + "\t password=" + password);
  }

  /**
   * java代码封装成json字符串
   * @throws JSONException
   */
  @Test
  public void javaToJSON() throws JSONException {
    System.out.println("java代码封装为json字符串");
    JSONObject jsonObj = new JSONObject();
    jsonObj.put("username", "张三");
    jsonObj.put("password", "");
    System.out.println("java--->json \n" + jsonObj.toString());
  }

  /**
   * json字符串转xml字符串
   */
  @Test
  public void jsonToXML() {
    System.out.println("json字符串转xml字符串");
    // JSON转XML
    String jsondata = "{\"root\":{" + "\"name\":\"zhaipuhong\","
        + "\"gender\":\"male\"," + "\"birthday\":{"
        + "\"year\":\"1970\"," + "\"month\":\"12\"," + "\"day\":\"17\""
        + "}" + "}" + "}";
    JSONObject jsonObject =JSONObject.fromObject(jsondata);
    String xmlstr = new XMLSerializer().write(jsonObject);
    System.out.println(xmlstr);
  }

  /**
   * xml字符串 转 json字符串
   */
  @Test
  public void xmlToJSON() {
    System.out.println("xml字符串转json字符串");
    String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><user_info><password></password><username>张三</username></user_info>";
    XMLSerializer xmlSerializer = new XMLSerializer();
    JSON json = xmlSerializer.read(xml);
    System.out.println("xml--->json \n" + json.toString());
  }

  /**
   * javabean转json字符串
   */
  @Test
  public void javaBeanToJSON() {
    System.out.println("javabean转json字符串");
    Person userInfo = new Person();
    userInfo.setName("张三");
    userInfo.setAge(20);
    JSONObject json = JSONObject.fromObject(userInfo);
    System.out.println("javabean--->json \n" + json.toString());
  }

  /**
   * javabean转xml字符串
   */
  @Test
  public void javaBeanToXML() {
    System.out.println("javabean转xml字符串");
    Person userInfo = new Person();
    userInfo.setName("张三");
    userInfo.setAge(20);
    net.sf.json.JSONObject json = net.sf.json.JSONObject.fromObject(userInfo);
    XMLSerializer xmlSerializer = new XMLSerializer();
    String xml = xmlSerializer.write((JSON) json, "UTF-8");
    System.out.println("javabean--->xml \n" + xml);
  }
}
